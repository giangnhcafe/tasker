import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators, AbstractControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit {

  @Input() isEdit: boolean = false;
  @Input() item: any;
  @Output('onSave') save = new EventEmitter<any>();
  @Output('onEdit') edit = new EventEmitter<any>();

  form!: FormGroup;
  
  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      id: [this.item?.id],
      name: [this.item?.name, Validators.required],
      description: [this.item?.description],
      dueDate: [this.item?.dueDate? this.item?.dueDate : moment(new Date()).format("DD/MM/YYYY")
      , [ValidateDate]],
      priority: [this.item?.priority ? this.item?.priority : 'NORMAL']
    });
  }

  get name() {
    return this.form.get('name');
  }
  get priority() { 
    return this.form.get('priority');
  }
  get dueDate() { 
    return this.form.get('dueDate');
  }

  emitSaveValue() {
    if(this.form.invalid) {return};
    if (!this.isEdit) {
      this.save.emit(this.form.value);
      this.form.reset({
        id: '',
        name: '',
        description: '',
        dueDate: moment(new Date()).format("DD/MM/YYYY"),
        priority: 'NORMAL'
      });
    } else {
      this.edit.emit(this.form.value);
    }
  }
}

function ValidateDate(control: AbstractControl): {[key: string]: any} | null  {
  const val = moment(control.value, "DD/MM/YYYY");
  const current = moment(new Date(), "DD/MM/YYYY").subtract(1, "days");
  if (val.isBefore(current)) { 
    return { invalidDate: true };
  }
  return null;
}

