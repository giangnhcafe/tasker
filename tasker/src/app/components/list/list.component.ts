import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import {
  debounceTime,
  map
} from "rxjs/operators";
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @ViewChild('searchInput', { static: true }) searchInput: any;
  @Input() items: any[] = [];
  @Output('onDelete') deleteEmit = new EventEmitter<any>();
  @Output('onSearch') searchEmit = new EventEmitter<any>();
  @Output('onEdit') editEmit = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      map((event: any) => {
        return event.target.value;
      })
      ,debounceTime(1000)
      ).subscribe((text: string) => {
        this.onSearch(text);
      });
  }

  select(item: any) { 
    this.items.find(ele => ele.checked === item.id).checked = !item.checked;
  }

  onExpand(item: any): void {
    item.isExpand = !item.isExpand;
  }

  onDelete(item: any) { 
    this.deleteEmit.emit(item?.id);
  }

  onSearch(text: string) { 
    this.searchEmit.emit(text)
  }

  onEdit(event: any) {
    this.editEmit.emit(event);
  }

  onDeleteSelect() { 
    const listRemove = this.items.filter((item) => item.checked);
    listRemove.forEach(item => { 
      this.deleteEmit.emit(item.id);
    })
  }
}
