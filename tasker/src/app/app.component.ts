import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  items = [];

  constructor(
    private service: AppService,
  ) {
  }

  ngOnInit() { 
    this.getAllTask();
  }

  getAllTask() { 
    this.service.get().subscribe((response) => { 
      this.items = response;
    });
  }

  onSearch(text: string) {
    if (text) {
      this.service.search(text).subscribe((response) => { 
        this.items = response;
      });
      return;
    } {
      this.getAllTask();
    }
    
  }

  onSave(event: any) { 
    if (event.id) {
      this.update(event);
    } else { 
      this.add(event);
    }
  }

  update(event: any) { 
    const body = {
      ...event,
      checked: false,
    }
    this.service.update(event.id, body).subscribe(() => {
      this.getAllTask();
    })
  }

  add(event: any) { 
    const body = {
      ...event,
      isExpand: false,
      checked: false,
    }
    this.service.add(body).subscribe(() => {
      this.getAllTask();
    })
  }

  onDelete(id: any) { 
    this.service.delete(id).subscribe((response) => { 
      this.getAllTask();
    });
  }
}
