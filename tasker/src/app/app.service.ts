import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {
  constructor(private http: HttpClient) { }

  get() {
    return this.http.get<any>('http://localhost:3000/task');
  }

  search(text: string) {
    return this.http.get<any>('http://localhost:3000/task?name=' + text);
  }

  add(body: any) { 
    return this.http.post<any>('http://localhost:3000/task', body);
  }

  update(id: number, body: any) { 
    return this.http.put<any>('http://localhost:3000/task/' + id, body);
  }

  delete(id: number) { 
    return this.http.delete<any>('http://localhost:3000/task/' + id);
  }
}